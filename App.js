import React, {Component} from 'react';
import { View} from 'react-native';
import CustomButton from './Components/CustomButton';
import CustomCalendar from './Components/CustomCalendar';

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 0.7}}>
          <CustomCalendar />
        </View>
        <View style={{flex: 0.3}}>
          <CustomButton
            buttonText={'Confirm'}
            color="#fe3650"
            textColor="white"
          />
        </View>
      </View>
    );
  }
}
